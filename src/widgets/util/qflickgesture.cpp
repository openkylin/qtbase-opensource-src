/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtWidgets module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl-3.0.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or (at your option) the GNU General
** Public license version 3 or any later version approved by the KDE Free
** Qt Foundation. The licenses are as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL2 and LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html and
** https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qgesture.h"
#include "qapplication.h"
#include "qevent.h"
#include "qwidget.h"
#if QT_CONFIG(graphicsview)
#include "qgraphicsitem.h"
#include "qgraphicsscene.h"
#include "qgraphicssceneevent.h"
#include "qgraphicsview.h"
#endif
#include "qstylehints.h"
#include "qscroller.h"
#include <QtGui/qtouchdevice.h>
#include "private/qapplication_p.h"
#include "private/qevent_p.h"
#include "private/qflickgesture_p.h"
#include "qdebug.h"
#include "qtimer.h"

#ifndef QT_NO_GESTURES

QT_BEGIN_NAMESPACE

//#define QFLICKGESTURE_DEBUG

#ifdef QFLICKGESTURE_DEBUG
#  define qFGDebug  qDebug
#else
#  define qFGDebug  while (false) qDebug
#endif

extern bool qt_sendSpontaneousEvent(QObject *receiver, QEvent *event);

QTimer* PressDelayHandler::m_pressDelayTimer;
bool PressDelayHandler::m_sendingEvent = false;
QScopedPointer<QEvent> PressDelayHandler::m_pressDelayEvent;
QPointer<QObject> PressDelayHandler::m_pressTarget;

PressDelayHandler::PressDelayHandler(QObject *parent)
    : QObject(parent)
{ }

PressDelayHandler* PressDelayHandler::create(Qt::MouseButton button)
{
    switch (button) {
    case Qt::LeftButton:
        return new MousePressDelayHandler(QCoreApplication::instance());
        break;
    case Qt::NoButton:
        return new TouchPressDelayHandler(QCoreApplication::instance());
    default:
        break;
    }
    return new MousePressDelayHandler(QCoreApplication::instance());
}

bool PressDelayHandler::shouldEventBeIgnored() const
{
    return m_sendingEvent;
}

bool PressDelayHandler::isDelaying() const
{
    return hasSavedDelayEvent();
}

void PressDelayHandler::pressed(QEvent *e, int delay)
{
    if (!hasSavedDelayEvent()) {
        qFGDebug("QFG: consuming/delaying press");
        m_pressDelayEvent.reset(copyEvent(e));
        saveDelayEventInfo(e);
        startTimer(delay);
    } else {
        qFGDebug("QFG: NOT consuming/delaying press");
    }

}

bool PressDelayHandler::released(QEvent *e, bool scrollerWasActive, bool scrollerIsActive)
{
    // stop the timer
    stopTimer();

    bool result = scrollerWasActive || scrollerIsActive;

    // we still haven't even sent the press, so do it now
    if (hasSavedDelayEvent() && pressTarget() && !scrollerIsActive) {

        qFGDebug() << "QFG: re-sending press (due to release) for " << pressTarget();
        sendSpontaneousEvent(m_pressDelayEvent.data(), pressTarget(), UngrabMouseBefore);

        qFGDebug() << "QFG: faking release (due to release) for " << pressTarget();
        sendSpontaneousEvent(e, pressTarget(), 0);

        result = true; // consume this event
    } else if (pressTarget() && scrollerIsActive) {
        // we grabbed the mouse expicitly when the scroller became active, so undo that now
        qFGDebug() << "QFG: ungrab mouse which grabbed when the scroller became active";
        sendSpontaneousEvent(nullptr, pressTarget(), UngrabMouseBefore);
    }

    resetDelayEvent();
    resetPressTarget();
    return result;
}

void PressDelayHandler::scrollerWasIntercepted()
{
    qFGDebug("QFG: deleting delayed press, since scroller was only intercepted");
    if (hasSavedDelayEvent()) {
        // we still haven't even sent the press, so just throw it away now
        stopTimer();
    }
    resetPressTarget();
}

void PressDelayHandler::scrollerBecameActive()
{
    if (hasSavedDelayEvent()) {
        // we still haven't even sent the press, so just throw it away now
        qFGDebug("QFG: deleting delayed mouse press, since scroller is active now");
        stopTimer();
        resetDelayEvent();
        resetPressTarget();
    } else if (pressTarget()) {
        // we did send a press, so we need to fake a release now
        releaseAllPressed();
        // don't clear the pressTarget just yet, since we need to explicitly ungrab the mouse on release!
    }
}

void PressDelayHandler::onDelayTimeout()
{
    if (hasSavedDelayEvent() && pressTarget()) {
        qFGDebug() << "QFG: timer event: re-sending press to " << pressTarget();
        sendSpontaneousEvent(m_pressDelayEvent.data(), pressTarget(), UngrabMouseBefore);
    }
    resetDelayEvent();

    stopTimer();
}

void PressDelayHandler::stopTimer()
{
    if (!m_pressDelayTimer)
        return;
    if (!m_pressDelayTimer->isActive())
        return;
    m_pressDelayTimer->stop();
}

void PressDelayHandler::startTimer(int timeout)
{
    if (!m_pressDelayTimer)
        m_pressDelayTimer = new QTimer;
    if (m_pressDelayTimer->isActive()) {
        qFGDebug() << "QFG: timer is active, don't start again";
        return;
    }
    m_pressDelayTimer->singleShot(timeout, this, &PressDelayHandler::onDelayTimeout);
}

void PressDelayHandler::sendSpontaneousEvent(QEvent *e, QObject *target, int flags)
{
#if QT_CONFIG(graphicsview)
    if (flags & UngrabMouseBefore)
        ungrabMouse(mouseGrabberItem());
#else
    Q_UNUSED(flags);
#endif

    m_sendingEvent = true;
    sendEvent(e, target);
    m_sendingEvent = false;

#if QT_CONFIG(graphicsview)
    if (flags & RegrabMouseAfterwards)
        grabMouse(mouseGrabberItem());
#endif
}

QPointer<QObject> PressDelayHandler::pressTarget() const
{
    return m_pressTarget;
}

void PressDelayHandler::setPressTarget(QObject* pressTarget)
{
    m_pressTarget = pressTarget;
}

void PressDelayHandler::resetPressTarget()
{
    m_pressTarget = nullptr;
}

bool PressDelayHandler::hasSavedDelayEvent() const
{
    return !m_pressDelayEvent.isNull();
}

void PressDelayHandler::resetDelayEvent()
{
    m_pressDelayEvent.reset(nullptr);
}

QGraphicsItem* PressDelayHandler::mouseGrabberItem() const
{
    QGraphicsItem *grabber = nullptr;

    QWidget* targetWidget = nullptr;
    if (pressTarget().data()->isWidgetType()) {
        targetWidget = dynamic_cast<QWidget*>(pressTarget().data());
    }
    else {
        targetWidget = QApplication::widgetAt(QCursor::pos());
    }
    if (!targetWidget)
        return nullptr;

    if (targetWidget->parentWidget()) {
        if (QGraphicsView *gv = qobject_cast<QGraphicsView *>(targetWidget->parentWidget())) {
            if (gv->scene())
                grabber = gv->scene()->mouseGrabberItem();
        }
    }
    return grabber;
}

void PressDelayHandler::grabMouse(QGraphicsItem* grabber) const
{
    if (!grabber)
        return;

    // GraphicsView Mouse Handling Workaround #2:
    // we need to re-grab the mouse after sending a faked mouse
    // release, since we still need the mouse moves for the gesture
    // (the scene will clear the item's mouse grabber status on
    // release).
    qFGDebug() << "QFG: re-grabbing" << grabber;
    grabber->grabMouse();
}

void PressDelayHandler::ungrabMouse(QGraphicsItem* grabber) const
{
    if (!grabber)
        return;

    // GraphicsView Mouse Handling Workaround #1:
    // we need to ungrab the mouse before re-sending the press,
    // since the scene had already set the mouse grabber to the
    // original (and consumed) event's receiver
    qFGDebug() << "QFG: ungrabbing" << grabber;
    grabber->ungrabMouse();
}

MousePressDelayHandler::MousePressDelayHandler(QObject *parent)
    : PressDelayHandler(parent)
    , m_mouseButton(Qt::NoButton)
    , m_mouseEventSource(Qt::MouseEventNotSynthesized)
{
    qFGDebug("QFG: create MousePressDelayHandler");
}

void MousePressDelayHandler::saveDelayEventInfo(QEvent* e)
{
    QMouseEvent* me = dynamic_cast<QMouseEvent*>(e);
    if (!me) {
        qWarning() << "MousePressDelayHandler handling event not QMouseEvent";
        return;
    }

    setPressTarget(QApplication::widgetAt(me->globalPos()));
    m_mouseButton = me->button();
    m_mouseEventSource = me->source();
}

QEvent* MousePressDelayHandler::copyEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonRelease:
    case QEvent::MouseMove: {
        QMouseEvent *me = dynamic_cast<QMouseEvent *>(e);
        QMouseEvent *cme = new QMouseEvent(me->type(), QPoint(0, 0), me->windowPos(), me->screenPos(),
                                           me->button(), me->buttons(), me->modifiers(), me->source());
        return cme;
    }
#if QT_CONFIG(graphicsview)
    case QEvent::GraphicsSceneMousePress:
    case QEvent::GraphicsSceneMouseRelease:
    case QEvent::GraphicsSceneMouseMove: {
        QGraphicsSceneMouseEvent *me = dynamic_cast<QGraphicsSceneMouseEvent *>(e);
#if 1
        QEvent::Type met = me->type() == QEvent::GraphicsSceneMousePress ? QEvent::MouseButtonPress :
                               (me->type() == QEvent::GraphicsSceneMouseRelease ? QEvent::MouseButtonRelease : QEvent::MouseMove);
        QMouseEvent *cme = new QMouseEvent(met, QPoint(0, 0), QPoint(0, 0), me->screenPos(),
                                           me->button(), me->buttons(), me->modifiers(), me->source());
        return cme;
#else
        QGraphicsSceneMouseEvent *copy = new QGraphicsSceneMouseEvent(me->type());
        copy->setPos(me->pos());
        copy->setScenePos(me->scenePos());
        copy->setScreenPos(me->screenPos());
        for (int i = 0x1; i <= 0x10; i <<= 1) {
            Qt::MouseButton button = Qt::MouseButton(i);
            copy->setButtonDownPos(button, me->buttonDownPos(button));
            copy->setButtonDownScenePos(button, me->buttonDownScenePos(button));
            copy->setButtonDownScreenPos(button, me->buttonDownScreenPos(button));
        }
        copy->setLastPos(me->lastPos());
        copy->setLastScenePos(me->lastScenePos());
        copy->setLastScreenPos(me->lastScreenPos());
        copy->setButtons(me->buttons());
        copy->setButton(me->button());
        copy->setModifiers(me->modifiers());
        copy->setSource(me->source());
        copy->setFlags(me->flags());
        return copy;
#endif
    }
#endif // QT_CONFIG(graphicsview)
    default:
        return nullptr;
    }
}

void MousePressDelayHandler::sendEvent(QEvent *e, QObject* target)
{
    QMouseEvent* me = dynamic_cast<QMouseEvent*>(e);
    QWidget* targetWidget = dynamic_cast<QWidget*>(target);

    if (!targetWidget) {
        return;
    }

    if (me) {
        QMouseEvent copy(me->type(), targetWidget->mapFromGlobal(me->globalPos()),
                         targetWidget->topLevelWidget()->mapFromGlobal(me->globalPos()), me->screenPos(),
                         me->button(), me->buttons(), me->modifiers(), me->source());
        qt_sendSpontaneousEvent(targetWidget, &copy);
    }
}

void MousePressDelayHandler::releaseAllPressed()
{
    QPoint farFarAway(-QWIDGETSIZE_MAX, -QWIDGETSIZE_MAX);

    qFGDebug() << "QFG: sending a fake mouse release at far-far-away to " << pressTarget();
    QMouseEvent re(QEvent::MouseButtonRelease, QPoint(), farFarAway, farFarAway,
                   m_mouseButton, QApplication::mouseButtons() & ~m_mouseButton,
                   QApplication::keyboardModifiers(), m_mouseEventSource);
    sendSpontaneousEvent(&re, pressTarget(), RegrabMouseAfterwards);
}

TouchPressDelayHandler::TouchPressDelayHandler(QObject *parent)
    : PressDelayHandler(parent)
{
    qFGDebug("QFG: create TouchPressDelayHandler");
}

void TouchPressDelayHandler::saveDelayEventInfo(QEvent* e)
{
    QTouchEvent* te = dynamic_cast<QTouchEvent*>(e);
    if (!te) {
        qWarning() << "TouchPressDelayHandler handling event not QTouchEvent";
    }
    setPressTarget(te->window());
    m_touchBeginPoint = te->touchPoints().at(0);
    m_device = te->device();
}

void TouchPressDelayHandler::releaseAllPressed()
{
    qFGDebug() << "QFG: sending a fake touch cancel to " << pressTarget();
    QTouchEvent te(QEvent::TouchCancel, m_device, QApplication::keyboardModifiers(),
                   Qt::TouchPointReleased, QList<QTouchEvent::TouchPoint>{m_touchBeginPoint});
    te.setWindow(dynamic_cast<QWindow*>(pressTarget().data()));
    sendSpontaneousEvent(&te, pressTarget(), 0);
}

void TouchPressDelayHandler::sendEvent(QEvent *e, QObject* target)
{
    QTouchEvent* te = dynamic_cast<QTouchEvent*>(e);
    QWindow* targetWindow = dynamic_cast<QWindow*>(target);

    if (!targetWindow) {
        return;
    }
    if (!te) {
        return;
    }

    // map to window pos for send to window
    QTouchEvent::TouchPoint touchPoint = (!te->touchPoints().empty()) ? te->touchPoints().at(0) : QTouchEvent::TouchPoint();
    const QPointF screenPos = touchPoint.screenPos();
    const QPointF delta = screenPos - screenPos.toPoint();
    const QPointF windowPos = targetWindow->mapFromGlobal(screenPos.toPoint()) + delta;
    const QPointF startPos = targetWindow->mapFromGlobal(touchPoint.startScreenPos().toPoint()) + delta;
    const QPointF lastPos = targetWindow->mapFromGlobal(touchPoint.lastScreenPos().toPoint()) + delta;

    touchPoint.setPos(windowPos);
    touchPoint.setStartPos(startPos);
    touchPoint.setLastPos(lastPos);

    qFGDebug() << "QFG: sending" << te->type() << "event to" << targetWindow;
    // send touch event to window because there is a grab mechanism for touch events
    QTouchEvent copy(te->type(), te->device(), te->modifiers(),
                     te->touchPointStates(), QList<QTouchEvent::TouchPoint>{touchPoint});
    copy.setWindow(targetWindow);
    qt_sendSpontaneousEvent(targetWindow, &copy);

    // when Qt::AA_SynthesizeMouseForUnhandledTouchEvents is set,
    // Qt will send a fake mouse eventif touch event not accept.
    // The touch event we send here will not send the fake mouse event,
    // so send it here.
    if ((qApp->testAttribute(Qt::AA_SynthesizeMouseForUnhandledTouchEvents) && !copy.isAccepted())
        || copy.type() == QEvent::TouchCancel) {
        sendMouseEventFromTouch(&copy, targetWindow);
    }
}

void TouchPressDelayHandler::sendMouseEventFromTouch(QTouchEvent *te, QWindow *target)
{
    if (te->type() == QEvent::TouchCancel) {
        QPoint farFarAway(-QWIDGETSIZE_MAX, -QWIDGETSIZE_MAX);

        qFGDebug() << "QFG: sending a fake mouse release at far-far-away to " << pressTarget();
        QMouseEvent fake(QEvent::MouseButtonRelease, QPoint(), farFarAway, farFarAway,
                       Qt::LeftButton, QApplication::mouseButtons() & ~Qt::LeftButton,
                       QApplication::keyboardModifiers(), Qt::MouseEventSynthesizedByQt);
        qt_sendSpontaneousEvent(target, &fake);
        return;
    }

    QTouchEvent::TouchPoint touchPoint = te->touchPoints().at(0);
    const QPointF screenPos = touchPoint.screenPos();
    const QPointF delta = screenPos - screenPos.toPoint();
    const QPointF pos = target->mapFromGlobal(screenPos.toPoint()) + delta;

    QEvent::Type mouseEventType = QEvent::MouseMove;
    Qt::MouseButton button = Qt::NoButton;
    Qt::MouseButtons buttons = Qt::LeftButton;

    switch (touchPoint.state()) {
    case Qt::TouchPointPressed:
        if (isDoubleClick()) {
            mouseEventType = QEvent::MouseButtonDblClick;
            button = Qt::NoButton;
            m_mousePressTime = QTime();
        }
        else {
            mouseEventType = QEvent::MouseButtonPress;
            button = Qt::LeftButton;
            m_mousePressTime = QTime::currentTime();
        }
        break;
    case Qt::TouchPointReleased:
        mouseEventType = QEvent::MouseButtonRelease;
        button = Qt::LeftButton;
        buttons = Qt::NoButton;
        break;
    default:
        break;
    }

    if (mouseEventType == QEvent::MouseButtonPress) {
        qFGDebug() << "QFG: send fake move event to" << target;
        QMouseEvent fake(QEvent::MouseMove, pos, pos, screenPos,
                         Qt::NoButton, Qt::NoButton,
                         QApplication::keyboardModifiers(), Qt::MouseEventSynthesizedByQt);
        qt_sendSpontaneousEvent(target, &fake);
    }

    // will not synthesize QEvent::NonClientAreaMouseButtonDblClick from touch
    qFGDebug() << "QFG: send fake " << mouseEventType << "to" << target;
    QMouseEvent fake(mouseEventType, pos, pos, screenPos,
                     button, buttons, QApplication::keyboardModifiers(), Qt::MouseEventSynthesizedByQt);
    qt_sendSpontaneousEvent(target, &fake);
}

bool TouchPressDelayHandler::isDoubleClick() const
{
    // if not save time, return false then save current time
    if (m_mousePressTime.isNull())
        return false;

    int doubleClickInterval = static_cast<int>(QGuiApplication::styleHints()->mouseDoubleClickInterval());
    int elapsed = m_mousePressTime.msecsTo(QTime::currentTime());
    bool doubleClick = elapsed < doubleClickInterval;

    return doubleClick;
}

QEvent* TouchPressDelayHandler::copyEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::TouchBegin:
    case QEvent::TouchUpdate:
    case QEvent::TouchEnd:
    case QEvent::TouchCancel: {
        QTouchEvent* te = dynamic_cast<QTouchEvent*>(e);
        QTouchEvent* copy = new QTouchEvent(te->type(), te->device(), te->modifiers(),
                                            te->touchPointStates(), te->touchPoints());
        copy->setWindow(te->window());
        return copy;
        break;
    }
    default:
        break;
    }

    return nullptr;
}

/*!
    \internal
    \class QFlickGesture
    \since 4.8
    \brief The QFlickGesture class describes a flicking gesture made by the user.
    \ingroup gestures
    The QFlickGesture is more complex than the QPanGesture that uses QScroller and QScrollerProperties
    to decide if it is triggered.
    This gesture is reacting on touch event as compared to the QMouseFlickGesture.

    \sa {Gestures in Widgets and Graphics View}, QScroller, QScrollerProperties, QMouseFlickGesture
*/

/*!
    \internal
*/
QFlickGesture::QFlickGesture(QObject *receiver, Qt::MouseButton button, QObject *parent)
    : QGesture(*new QFlickGesturePrivate, parent)
{
    d_func()->q_ptr = this;
    d_func()->receiver = receiver;
    d_func()->receiverScroller = (receiver && QScroller::hasScroller(receiver)) ? QScroller::scroller(receiver) : 0;
    d_func()->button = button;
    d_func()->pressDelayHandler = PressDelayHandler::create(button);
}

QFlickGesture::~QFlickGesture()
{ }

QFlickGesturePrivate::QFlickGesturePrivate()
    : receiverScroller(0), button(Qt::NoButton), macIgnoreWheel(false), pressDelayHandler(nullptr)
{ }


//
// QFlickGestureRecognizer
//


QFlickGestureRecognizer::QFlickGestureRecognizer(Qt::MouseButton button)
{
    this->button = button;
}

/*! \reimp
 */
QGesture *QFlickGestureRecognizer::create(QObject *target)
{
#if QT_CONFIG(graphicsview)
    QGraphicsObject *go = qobject_cast<QGraphicsObject*>(target);
    if (go && button == Qt::NoButton) {
        go->setAcceptTouchEvents(true);
    }
#endif
    return new QFlickGesture(target, button);
}

/*! \internal
    The recognize function detects a touch event suitable to start the attached QScroller.
    The QFlickGesture will be triggered as soon as the scroller is no longer in the state
    QScroller::Inactive or QScroller::Pressed. It will be finished or canceled
    at the next QEvent::TouchEnd.
    Note that the QScroller might continue scrolling (kinetically) at this point.
 */
QGestureRecognizer::Result QFlickGestureRecognizer::recognize(QGesture *state,
                                                              QObject *watched,
                                                              QEvent *event)
{
    Q_UNUSED(watched);

    static QElapsedTimer monotonicTimer;
    if (!monotonicTimer.isValid())
        monotonicTimer.start();

    QFlickGesture *q = static_cast<QFlickGesture *>(state);
    QFlickGesturePrivate *d = q->d_func();

    QScroller *scroller = d->receiverScroller;
    if (!scroller)
        return Ignore; // nothing to do without a scroller?

    QWidget *receiverWidget = qobject_cast<QWidget *>(d->receiver);
#if QT_CONFIG(graphicsview)
    QGraphicsObject *receiverGraphicsObject = qobject_cast<QGraphicsObject *>(d->receiver);
#endif

    // this is only set for events that we inject into the event loop via sendEvent()
    if (d->pressDelayHandler->shouldEventBeIgnored()) {
        //qFGDebug() << state << "QFG: ignored event: " << event->type();
        return Ignore;
    }

    const QMouseEvent *me = 0;
#if QT_CONFIG(graphicsview)
    const QGraphicsSceneMouseEvent *gsme = 0;
#endif
    const QTouchEvent *te = 0;
    QPoint globalPos;

    // qFGDebug() << "FlickGesture "<<state<<"watched:"<<watched<<"receiver"<<d->receiver<<"event"<<event->type()<<"button"<<button;

    switch (event->type()) {
    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonRelease:
    case QEvent::MouseMove:
        if (!receiverWidget)
            return Ignore;
        if (button != Qt::NoButton) {
            me = static_cast<const QMouseEvent *>(event);
            globalPos = me->globalPos();
        }
        break;
#if QT_CONFIG(graphicsview)
    case QEvent::GraphicsSceneMousePress:
    case QEvent::GraphicsSceneMouseRelease:
    case QEvent::GraphicsSceneMouseMove:
        if (!receiverGraphicsObject)
            return Ignore;
        if (button != Qt::NoButton) {
            gsme = static_cast<const QGraphicsSceneMouseEvent *>(event);
            globalPos = gsme->screenPos();
        }
        break;
#endif
    case QEvent::TouchBegin:
    case QEvent::TouchEnd:
    case QEvent::TouchUpdate:
        if (button == Qt::NoButton) {
            te = static_cast<const QTouchEvent *>(event);
            if (!te->touchPoints().isEmpty())
                globalPos = te->touchPoints().at(0).screenPos().toPoint();
        }
        break;

    // consume all wheel events if the scroller is active
    case QEvent::Wheel:
        if (d->macIgnoreWheel || (scroller->state() != QScroller::Inactive))
            return Ignore | ConsumeEventHint;
        break;

    // consume all dbl click events if the scroller is active
    case QEvent::MouseButtonDblClick:
        if (scroller->state() != QScroller::Inactive)
            return Ignore | ConsumeEventHint;
        break;

    default:
        break;
    }

    if (!me
#if QT_CONFIG(graphicsview)
        && !gsme
#endif
        && !te) // Neither mouse nor touch
        return Ignore;

    // get the current pointer position in local coordinates.
    QPointF point;
    QScroller::Input inputType = (QScroller::Input) 0;

    switch (event->type()) {
    case QEvent::MouseButtonPress:
        if (me && me->button() == button && me->buttons() == button) {
            point = me->globalPos();
            inputType = QScroller::InputPress;
        } else if (me) {
            scroller->stop();
            return CancelGesture;
        }
        break;
    case QEvent::MouseButtonRelease:
        if (me && me->button() == button) {
            point = me->globalPos();
            inputType = QScroller::InputRelease;
        }
        break;
    case QEvent::MouseMove:
        if (me && me->buttons() == button) {
            point = me->globalPos();
            inputType = QScroller::InputMove;
        }
        break;

#if QT_CONFIG(graphicsview)
    case QEvent::GraphicsSceneMousePress:
        if (gsme && gsme->button() == button && gsme->buttons() == button) {
            point = gsme->scenePos();
            inputType = QScroller::InputPress;
        } else if (gsme) {
            scroller->stop();
            return CancelGesture;
        }
        break;
    case QEvent::GraphicsSceneMouseRelease:
        if (gsme && gsme->button() == button) {
            point = gsme->scenePos();
            inputType = QScroller::InputRelease;
        }
        break;
    case QEvent::GraphicsSceneMouseMove:
        if (gsme && gsme->buttons() == button) {
            point = gsme->scenePos();
            inputType = QScroller::InputMove;
        }
        break;
#endif

    case QEvent::TouchBegin:
        inputType = QScroller::InputPress;
        Q_FALLTHROUGH();
    case QEvent::TouchEnd:
        if (!inputType)
            inputType = QScroller::InputRelease;
        Q_FALLTHROUGH();
    case QEvent::TouchUpdate:
        if (!inputType)
            inputType = QScroller::InputMove;

        if (te->device()->type() == QTouchDevice::TouchPad) {
            if (te->touchPoints().count() != 2)  // 2 fingers on pad
                return Ignore;

            point = te->touchPoints().at(0).startScenePos() +
                    ((te->touchPoints().at(0).scenePos() - te->touchPoints().at(0).startScenePos()) +
                     (te->touchPoints().at(1).scenePos() - te->touchPoints().at(1).startScenePos())) / 2;
        } else { // TouchScreen
            if (te->touchPoints().count() != 1) // 1 finger on screen
                return Ignore;

            point = te->touchPoints().at(0).scenePos();
        }
        break;

    default:
        break;
    }

    // Check for an active scroller at globalPos
    if (inputType == QScroller::InputPress) {
        const auto activeScrollers = QScroller::activeScrollers();
        for (QScroller *as : activeScrollers) {
            if (as != scroller) {
                QRegion scrollerRegion;

                if (QWidget *w = qobject_cast<QWidget *>(as->target())) {
                    scrollerRegion = QRect(w->mapToGlobal(QPoint(0, 0)), w->size());
#if QT_CONFIG(graphicsview)
                } else if (QGraphicsObject *go = qobject_cast<QGraphicsObject *>(as->target())) {
                    if (const auto *scene = go->scene()) {
                        const auto goBoundingRectMappedToScene = go->mapToScene(go->boundingRect());
                        const auto views = scene->views();
                        for (QGraphicsView *gv : views) {
                            scrollerRegion |= gv->mapFromScene(goBoundingRectMappedToScene)
                                              .translated(gv->mapToGlobal(QPoint(0, 0)));
                        }
                    }
#endif
                }
                // active scrollers always have priority
                if (scrollerRegion.contains(globalPos))
                    return Ignore;
            }
        }
    }

    bool scrollerWasDragging = (scroller->state() == QScroller::Dragging);
    bool scrollerWasScrolling = (scroller->state() == QScroller::Scrolling);

    if (inputType) {
        if (QWidget *w = qobject_cast<QWidget *>(d->receiver))
            point = w->mapFromGlobal(point.toPoint());
#if QT_CONFIG(graphicsview)
        else if (QGraphicsObject *go = qobject_cast<QGraphicsObject *>(d->receiver))
            point = go->mapFromScene(point);
#endif

        // inform the scroller about the new event
        scroller->handleInput(inputType, point, monotonicTimer.elapsed());
    }

    // depending on the scroller state return the gesture state
    Result result;
    bool scrollerIsActive = (scroller->state() == QScroller::Dragging ||
                             scroller->state() == QScroller::Scrolling);

    // Consume all mouse events while dragging or scrolling to avoid nasty
    // side effects with Qt's standard widgets.
    if ((me
#if QT_CONFIG(graphicsview)
         || gsme
#endif
         || te) && scrollerIsActive)
        result |= ConsumeEventHint;

    // The only problem with this approach is that we consume the
    // MouseRelease when we start the scrolling with a flick gesture, so we
    // have to fake a MouseRelease "somewhere" to not mess with the internal
    // states of Qt's widgets (a QPushButton would stay in 'pressed' state
    // forever, if it doesn't receive a MouseRelease).
    if (me
#if QT_CONFIG(graphicsview)
        || gsme
#endif
        || te) {
        if (!scrollerWasDragging && !scrollerWasScrolling && scrollerIsActive)
            d->pressDelayHandler->scrollerBecameActive();
        else if (scrollerWasScrolling && (scroller->state() == QScroller::Dragging || scroller->state() == QScroller::Inactive))
            d->pressDelayHandler->scrollerWasIntercepted();
    }

    if (!inputType) {
        result |= Ignore;
    } else {
        switch (event->type()) {
        case QEvent::MouseButtonPress:
        case QEvent::TouchBegin:
#if QT_CONFIG(graphicsview)
        case QEvent::GraphicsSceneMousePress:
#endif
            if (scroller->state() == QScroller::Pressed) {
                int pressDelay = int(1000 * scroller->scrollerProperties().scrollMetric(QScrollerProperties::MousePressEventDelay).toReal());
                if (pressDelay > 0) {
                    result |= ConsumeEventHint;

                    d->pressDelayHandler->pressed(event, pressDelay);
                    event->accept();
                }
            }
            q->setHotSpot(globalPos);
            result |= scrollerIsActive ? TriggerGesture : MayBeGesture;
            break;

        case QEvent::MouseMove:
        case QEvent::TouchUpdate:
#if QT_CONFIG(graphicsview)
        case QEvent::GraphicsSceneMouseMove:
#endif
            if (d->pressDelayHandler->isDelaying())
                result |= ConsumeEventHint;
            result |= scrollerIsActive ? TriggerGesture : Ignore;
            break;

#if QT_CONFIG(graphicsview)
        case QEvent::GraphicsSceneMouseRelease:
#endif
        case QEvent::MouseButtonRelease:
        case QEvent::TouchEnd:
            if (d->pressDelayHandler->released(event, scrollerWasDragging || scrollerWasScrolling, scrollerIsActive))
                result |= ConsumeEventHint;
            result |= scrollerIsActive ? FinishGesture : CancelGesture;
            break;

        default:
            result |= Ignore;
            break;
        }
    }
    return result;
}


/*! \reimp
 */
void QFlickGestureRecognizer::reset(QGesture *state)
{
    QGestureRecognizer::reset(state);
}

QT_END_NAMESPACE

#include "moc_qflickgesture_p.cpp"

#endif // QT_NO_GESTURES
